<?php

	use PHPUnit\Framework\TestCase;
	use ScopeRecruiting\ScopeRestApi;

	class ScopeRestApiTest extends TestCase {
		private $env = [
			'SCOPE_CLIENT_ID' => 12,
			'SCOPE_CLIENT_SECRET' => 'ea8ad038561214410eef00b3d990e5b580cf9037e67047ab0b4869605660e2c5',
			'SCOPE_URL' => 'https://oleg.scope-recruiting.de/app/',
		];

		private $applicant_auth_data = [
			'email'    => 'oleg@artrevolver.de',
			'password' => 'foobar'
		];

		public function testConstructorSuccess() {
			$scope = new ScopeRestApi($this->env);
			$this->assertInstanceOf("ScopeRecruiting\ScopeRestApi", $scope);

			return $scope;
		}

		public function testConstructorFail() {
			$this->expectException(InvalidArgumentException::class);
			new ScopeRestApi([]);
		}


		public function testCompanyAuth() {

			$scope = $this->getMockBuilder("ScopeRecruiting\ScopeRestApi")
				->setMethods(['__construct'])
				->setConstructorArgs($this->env)
				->disableOriginalConstructor()
			    ->getMock();

			$scope->scope_url = $this->env['SCOPE_URL'];

			$scope->setCompanyAuthConfig($this->env);
			$scope->authenticateCompany();

			$companyAuthData = $scope->getCompanyAuthData();

			$this->assertIsArray($companyAuthData);
			$this->assertEquals(2, count($companyAuthData));
			$this->assertArrayHasKey('token', $companyAuthData);
			$this->assertArrayHasKey('expirationDate', $companyAuthData);
			$this->assertGreaterThan(strtotime(date('Y-m-d H:i:s')), strtotime($companyAuthData['expirationDate']));
		}

		/**
		 * @param ScopeRestApi $scope
		 *
		 * @depends testConstructorSuccess
		 *
		 * @throws Exception on auth error
		 * @throws Token has expired
		 *
		 */

		public function testApplicantAuth(ScopeRestApi $scope) {
			$scope->setApplicantAuthConfig($this->applicant_auth_data['email'], $this->applicant_auth_data['password']);

			$scope->authenticateApplicant();

			$applicantAuthData = $scope->getApplicantAuthData();

			$this->assertIsArray($applicantAuthData);
			$this->assertEquals(3, count($applicantAuthData));

			$this->assertArrayHasKey('applicant_email', $applicantAuthData);
			$this->assertArrayHasKey('applicant_signature', $applicantAuthData);
			$this->assertArrayHasKey('timestamp', $applicantAuthData);

			$this->assertNull($applicantAuthData['applicant_email']);
			$this->assertNull($applicantAuthData['applicant_signature']);
			$this->assertNull($applicantAuthData['timestamp']);
		}


		public function printH($data) {
			echo "<pre>";
			print_r($data);
			echo "</pre>";
			die;
		}
	}
